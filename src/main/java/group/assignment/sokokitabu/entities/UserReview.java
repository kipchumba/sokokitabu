package group.assignment.sokokitabu.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the user_review database table.
 * 
 */
@Entity
@Table(name = "User_Review")
@NamedQueries({
	@NamedQuery(name = "UserReview.findAll", query = "SELECT u FROM UserReview u"),
	@NamedQuery(name = "UserReview.findByRating", query = "SELECT u FROM UserReview u WHERE u.reviewerRating = :rating")

})
public class UserReview implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private UserReviewPK id;

    @Lob
    private String review;

    private int reviewerRating;

    // bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name = "ReviewedUserId")
    private User reviewed;

    // bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name = "ReviewerId")
    private User reviewer;

    public UserReview() {
    }

    
    public UserReview(String review, int reviewerRating, User reviewed,
	    User reviewer) {
	this.review = review;
	this.reviewerRating = reviewerRating;
	this.reviewed = reviewed;
	this.reviewer = reviewer;
    }


    public UserReviewPK getId() {
	return this.id;
    }

    public void setId(UserReviewPK id) {
	this.id = id;
    }

    public String getReview() {
	return this.review;
    }

    public void setReview(String review) {
	this.review = review;
    }

    public int getReviewerRating() {
	return this.reviewerRating;
    }

    public void setReviewerRating(int reviewerRating) {
	this.reviewerRating = reviewerRating;
    }

    public User getReviewed() {
	return reviewed;
    }

    public void setReviewed(User reviewed) {
	this.reviewed = reviewed;
    }

    public User getReviewer() {
	return reviewer;
    }

    public void setReviewer(User reviewer) {
	this.reviewer = reviewer;
    }

}