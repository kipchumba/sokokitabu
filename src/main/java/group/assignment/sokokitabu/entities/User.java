package group.assignment.sokokitabu.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = User.FIND_ALL, query = "SELECT u FROM User u"),
		@NamedQuery(name = User.FIND_BY_EMAIL, query = "SELECT u FROM User u WHERE u.email = :email"),
		@NamedQuery(name = User.FIND_BY_USERNAME, query = "SELECT u FROM User u WHERE u.username = :username"),
		@NamedQuery(name = User.FIND_BY_EMAIL_AND_PASWWORD, query = "SELECT u FROM User u WHERE (u.email = :email AND u.password = :password)"),
		@NamedQuery(name = User.FIND_BY_USERNAME_AND_PASSWORD, query = "SELECT u FROM User u WHERE (u.username = :username AND u.password = :password)")

})
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String email;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String gender;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String username;

	// bi-directional one-to-one association to Address
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private Address address;

	// bi-directional many-to-many association to Book
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "user_wishlist", joinColumns = { @JoinColumn(name = "UserId") }, inverseJoinColumns = { @JoinColumn(name = "BookId") })
	private List<Book> booksInWishList;

	// bi-directional many-to-one association to UserReview
	@OneToMany(mappedBy = "reviewer", cascade = CascadeType.PERSIST)
	private List<UserReview> reviewsOfOthers;

	// bi-directional many-to-one association to UserReview
	@OneToMany(mappedBy = "reviewed", cascade = CascadeType.PERSIST)
	private List<UserReview> reviewsByOthers;

	// bi-directional many-to-one association to Userinbox
	@OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
	private List<UserInbox> userInboxes;

	// bi-directional many-to-one association to UserBookPrice
	@OneToMany(mappedBy = "user", cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	private List<UserBook> userBooks;

	// bi-directional many-to-one association to BookRequest
	@OneToMany(mappedBy = "owner")
	private List<BookRequest> bookRequestsAsOwner;

	// bi-directional many-to-one association to BookRequest
	@OneToMany(mappedBy = "requester")
	private List<BookRequest> bookRequestsAsRequester;

	public static final String FIND_ALL = "User.findAll";
	public static final String FIND_BY_EMAIL = "User.findByEmai";
	public static final String FIND_BY_USERNAME = "User.findByUsername";
	public static final String FIND_BY_USERNAME_AND_PASSWORD = "User.findByUsernameAndPassword";
	public static final String FIND_BY_EMAIL_AND_PASWWORD = "User.findByEmailAndPassword";

	public User() {
	}

	public User(String email, String firstName, String gender, String lastName,
			String password, String username) {
		this.email = email;
		this.firstName = firstName;
		this.gender = gender;
		this.lastName = lastName;
		this.password = password;
		this.username = username;
	}

	public User(String email, String firstName, String gender, String lastName,
			String password, String username, Address address) {
		this.email = email;
		this.firstName = firstName;
		this.gender = gender;
		this.lastName = lastName;
		this.password = password;
		this.username = username;
		this.address = address;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Book> getBooksInWishList() {
		return booksInWishList;
	}

	public void setBooksInWishList(List<Book> booksInWishList) {
		this.booksInWishList = booksInWishList;
	}

	public Book addBookInWishList(Book book) {
		getBooksInWishList().add(book);
		return book;
	}

	public Book removeBookInWishList(Book book) {
		getBooksInWishList().remove(book);
		return book;
	}

	public List<UserReview> getReviewsByOthers() {
		return reviewsByOthers;
	}

	public void setReviewsByOthers(List<UserReview> reviewsByOthers) {
		this.reviewsByOthers = reviewsByOthers;
	}

	public void setUserReviews(List<UserReview> userReviews) {
		this.reviewsOfOthers = userReviews;
	}

	public UserReview addReviewByOther(UserReview userReview) {
		getReviewsByOthers().add(userReview);
		userReview.setReviewer(this);
		return userReview;
	}

	public UserReview removeReviewByOther(UserReview userReview) {
		getReviewsByOthers().remove(userReview);
		userReview.setReviewer(null);
		return userReview;
	}

	public List<UserReview> getReviewsOfOthers() {
		return reviewsOfOthers;
	}

	public void setReviewsOfOthers(List<UserReview> reviewsOfOthers) {
		this.reviewsOfOthers = reviewsOfOthers;
	}

	public UserReview addReviewOfOther(UserReview userReview) {
		getReviewsOfOthers().add(userReview);
		userReview.setReviewed(this);
		return userReview;
	}

	public UserReview removeReviewOfOther(UserReview userReview) {
		getReviewsOfOthers().remove(userReview);
		userReview.setReviewed(null);
		return userReview;
	}

	public List<UserInbox> getUserInboxes() {
		return userInboxes;
	}

	public void setUserInboxes(List<UserInbox> userInboxes) {
		this.userInboxes = userInboxes;
	}

	public UserInbox addUserInbox(UserInbox userInbox) {
		getUserInboxes().add(userInbox);
		userInbox.setUser(this);
		return userInbox;
	}

	public UserInbox removeUserInbox(UserInbox userInbox) {
		getUserInboxes().remove(userInbox);
		userInbox.setUser(null);
		return userInbox;
	}

	public List<UserBook> getUserBooks() {
		return userBooks;
	}

	public void setUserBooks(List<UserBook> userBooks) {
		this.userBooks = userBooks;
	}

	public UserBook addUserBook(UserBook userBook) {
		getUserBooks().add(userBook);
		return userBook;
	}

	public UserBook removeUserBook(UserBook userBook) {
		getUserBooks().remove(userBook);
		return userBook;
	}

	public List<BookRequest> getBookRequestsAsOwner() {
		return bookRequestsAsOwner;
	}

	public void setBookRequestsAsOwner(List<BookRequest> bookRequestsAsOwner) {
		this.bookRequestsAsOwner = bookRequestsAsOwner;
	}

	public BookRequest addBookRequestAsOwner(BookRequest bookRequest) {
		getBookRequestsAsOwner().add(bookRequest);
		bookRequest.setOwner(this);
		return bookRequest;
	}

	public BookRequest removeBookRequestAsOwner(BookRequest bookRequest) {
		getBookRequestsAsOwner().remove(bookRequest);
		bookRequest.setOwner(null);
		return bookRequest;
	}

	public List<BookRequest> getBookRequestsAsRequester() {
		return bookRequestsAsRequester;
	}

	public void setBookRequestsAsRequester(
			List<BookRequest> bookRequestsAsRequester) {
		this.bookRequestsAsRequester = bookRequestsAsRequester;
	}

	public BookRequest addBookRequestAsRequester(BookRequest bookRequest) {
		getBookRequestsAsRequester().add(bookRequest);
		bookRequest.setRequester(this);
		return bookRequest;
	}

	public BookRequest removeBookRequestAsRequester(BookRequest bookRequest) {
		getBookRequestsAsRequester().remove(bookRequest);
		bookRequest.setRequester(null);
		return bookRequest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}