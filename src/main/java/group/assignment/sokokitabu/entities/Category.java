package group.assignment.sokokitabu.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;

/**
 * The persistent class for the category database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Category.FIND_ALL, query = "SELECT c FROM Category c"),
	@NamedQuery(name = Category.FIND_BY_LEVEL, query = "SELECT c FROM Category c WHERE c.level = :level"),
	@NamedQuery(name = Category.FIND_BY_LEVEL_AND_SUBLEVEL, query = "SELECT c FROM Category c WHERE (c.level = :level AND c.subLevel = :sublevel)"),
	@NamedQuery(name = Category.FIND_BY_LEVEL_SUBLEVEL_AND_SUBJECT, query = "SELECT c FROM Category c WHERE (c.level = :level AND c.subLevel = :sublevel AND c.subject = :subject)"),
	@NamedQuery(name = Category.FIND_DISTINCT_SUBLEVELS_OF_LEVEL, query = "SELECT DISTINCT c.subLevel FROM Category c WHERE c.level = :level"),
	@NamedQuery(name =Category.FIND_DISTINCT_SUBJECTS_OF_LEVEL, query = "SELECT DISTINCT c.subject FROM Category c WHERE c.level = :level"),
	@NamedQuery(name = Category.FIND_BY_LEVEL_AND_SUBJECT, query = "SELECT c FROM Category c WHERE c.level = :level AND c.subject = :subject")
})
public class Category implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private Level level;

    private String subject;

    private String subLevel;

    // bi-directional many-to-one association to Book
    @OneToMany(mappedBy = "category")
    private List<Book> books;

    public final static String FIND_ALL = "Category.findAll";
    public final static String FIND_BY_LEVEL = "CategoryfindByLevel";
    public final static String FIND_BY_LEVEL_AND_SUBLEVEL = "CategoryfindByLevelAndSubLevel";
    public final static String FIND_BY_LEVEL_SUBLEVEL_AND_SUBJECT = "Category.findByLevelSubLevelAndSubject";
    public final static String FIND_DISTINCT_SUBLEVELS_OF_LEVEL = "FindDistinctSublevelsOfLevel";
    public final static String FIND_DISTINCT_SUBJECTS_OF_LEVEL = "FindDistinctSubjectsOfLevel";
    public final static String FIND_BY_LEVEL_AND_SUBJECT ="FindByLevelAndSubject";
    

    public Category() {
    }

    public Category(Level level, String subject, String subLevel) {
	this.level = level;
	this.subject = subject;
	this.subLevel = subLevel;
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Level getLevel() {
	return this.level;
    }

    public void setLevel(Level level) {
	this.level = level;
    }

    public String getSubject() {
	return this.subject;
    }

    public void setSubject(String subject) {
	this.subject = subject;
    }

    public String getSubLevel() {
	return this.subLevel;
    }

    public void setSubLevel(String subLevel) {
	this.subLevel = subLevel;
    }

    public List<Book> getBooks() {
	return this.books;
    }

    public void setBooks(List<Book> books) {
	this.books = books;
    }

    public Book addBook(Book book) {
	getBooks().add(book);
	book.setCategory(this);

	return book;
    }

    public Book removeBook(Book book) {
	getBooks().remove(book);
	book.setCategory(null);

	return book;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((level == null) ? 0 : level.hashCode());
	result = prime * result + ((subLevel == null) ? 0 : subLevel.hashCode());
	result = prime * result + ((subject == null) ? 0 : subject.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Category other = (Category) obj;
	if (level != other.level)
	    return false;
	if (subLevel != other.subLevel)
	    return false;
	if (subject == null) {
	    if (other.subject != null)
		return false;
	} else if (!subject.equals(other.subject))
	    return false;
	return true;
    }

    
}