package group.assignment.sokokitabu.entities;

public enum Level {
    Primary("Primary School"), Secondary("Secondary School"), Higher(
	    "Higher Education"), Recreational("Recreational");
    private String name;

    private Level(String name) {
	this.name = name;
    }
    public String getName() {
	return name;
    }

}
