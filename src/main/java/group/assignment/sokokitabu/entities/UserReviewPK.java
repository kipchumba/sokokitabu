package group.assignment.sokokitabu.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the user_review database table.
 * 
 */
@Embeddable
public class UserReviewPK implements Serializable {
    // default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(insertable = false, updatable = false)
    private String reviewerId;

    @Column(insertable = false, updatable = false)
    private String reviewedUserId;

    public UserReviewPK() {
    }

    public String getReviewerId() {
	return this.reviewerId;
    }

    public void setReviewerId(String reviewerId) {
	this.reviewerId = reviewerId;
    }

    public String getReviewedUserId() {
	return this.reviewedUserId;
    }

    public void setReviewedUserId(String reviewedUserId) {
	this.reviewedUserId = reviewedUserId;
    }

    public boolean equals(Object other) {
	if (this == other) {
	    return true;
	}
	if (!(other instanceof UserReviewPK)) {
	    return false;
	}
	UserReviewPK castOther = (UserReviewPK) other;
	return this.reviewerId.equals(castOther.reviewerId)
		&& this.reviewedUserId.equals(castOther.reviewedUserId);
    }

    public int hashCode() {
	final int prime = 31;
	int hash = 17;
	hash = hash * prime + this.reviewerId.hashCode();
	hash = hash * prime + this.reviewedUserId.hashCode();

	return hash;
    }
}