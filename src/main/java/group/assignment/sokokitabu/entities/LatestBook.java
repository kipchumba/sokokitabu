package group.assignment.sokokitabu.entities;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the latest_books database table.
 * 
 */
@Entity
@Table(name = "Latest_Books")
@NamedQuery(name = LatestBook.FIND_ALL, query = "SELECT l FROM LatestBook l ORDER BY l.timeAdded DESC")
public class LatestBook implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Date timeAdded;

    // bi-directional many-to-one association to Book
    @ManyToOne
    @JoinColumn(name = "BookId")
    private Book book;
    
    public final static String FIND_ALL = "LatestBook.findAll";

    public LatestBook() {
    }

    public Book getBook() {
	return this.book;
    }

    public void setBook(Book book) {
	this.book = book;
    }

    public java.util.Date getTimeAdded() {
	return timeAdded;
    }

    public void setTimeAdded(java.util.Date timeAdded) {
	this.timeAdded = timeAdded;
    }

}