package group.assignment.sokokitabu.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 * The persistent class for the book database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = Book.FIND_ALL, query = "SELECT b FROM Book b"),
		@NamedQuery(name = Book.FIND_BY_TITLE_LIKE, query = "SELECT b FROM Book b WHERE b.title LIKE :title"),
		@NamedQuery(name = Book.FIND_BY_PUBLISHER_LIKE, query = "SELECT b FROM Book b WHERE b.publisher LIKE :publisher"),
		@NamedQuery(name = Book.FIND_BY_LEVEL, query = "SELECT b FROM Book b WHERE b.category.level = :level"),
		@NamedQuery(name = Book.FIND_BY_SUBLEVEL, query = "SELECT b FROM Book b WHERE b.category.subLevel = :sublevel"),
		@NamedQuery(name = Book.FIND_BY_SUBJECT, query = "SELECT b FROM Book b WHERE b.category.subject = :subject"),
		@NamedQuery(name = Book.FIND_BY_LEVEL_AND_SUBLEVEL, query = "SELECT b FROM Book b WHERE (b.category.level = :level AND b.category.subLevel = :sublevel)"),
		@NamedQuery(name = Book.FIND_BY_SUBLEVEL_AND_SUBJECT, query = "SELECT b FROM Book b WHERE (b.category.subLevel = :sublevel AND b.category.subject = :subject)"),
		@NamedQuery(name = Book.FIND_BY_LEVEL_SUBLEVEL_AND_SUBJECT, query = "SELECT b FROM Book b WHERE (b.category.level = :level AND b.category.subLevel = :sublevel AND b.category.subject = :subject)"),
		@NamedQuery(name = Book.FIND_RECENTLY_ADDED, query = "SELECT b FROM Book b WHERE b.category.subject = :subject"),
		@NamedQuery(name = Book.FIND_BY_ALL, query = "SELECT b FROM Book b WHERE (b.title = :title AND b.publisher = :publisher AND b.category = :category)"),
		@NamedQuery(name = Book.FIND_BY_LEVEL_AND_SUBJECT, query = "SELECT b FROM Book b WHERE (b.category.level = :level AND b.category.subject = :subject)") })
public class Book implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	private Integer id;

	private String publisher;

	private String title;

	// bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name = "CategoryId")
	private Category category;

	// bi-directional many-to-many association to User
	@ManyToMany(mappedBy = "booksInWishList", cascade = { CascadeType.REFRESH,
			CascadeType.PERSIST })
	private List<User> wishfulUsers;

	// bi-directional many-to-one association to UserBookPrice
	@OneToMany(mappedBy = "book")
	private List<UserBook> userBooks;

	// bi-directional many-to-one association to BookRequest
	@OneToMany(mappedBy = "book")
	private List<BookRequest> bookRequests;

	public final static String FIND_ALL = "Book.findAll";
	public final static String FIND_BY_TITLE_LIKE = "Book.findByTitleLike";
	public final static String FIND_BY_PUBLISHER_LIKE = "Book.findByPublisherLike";
	public final static String FIND_BY_LEVEL = "Book.findByLevel";
	public final static String FIND_BY_SUBLEVEL = "Book.findBySublevel";
	public final static String FIND_BY_SUBJECT = "Book.findBySubject";
	public final static String FIND_BY_LEVEL_AND_SUBLEVEL = "Book.findByLevelAndSublevel";
	public final static String FIND_BY_SUBLEVEL_AND_SUBJECT = "Book.findBySublevelAndSubject";
	public final static String FIND_BY_LEVEL_SUBLEVEL_AND_SUBJECT = "Book.findByLevelSublevelAndSubject";
	public final static String FIND_RECENTLY_ADDED = "Book.findRecentlyAdded";
	public final static String FIND_BY_ALL = "Book.findByAll";
	public final static String FIND_BY_LEVEL_AND_SUBJECT = "Book.findByLevelAndSubject";

	public Book() {
	}

	public Book(String publisher, String title, Category category) {
		this.publisher = publisher;
		this.title = title;
		this.category = category;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPublisher() {
		return this.publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<User> getWishfulUsers() {
		return wishfulUsers;
	}

	public void setWishfulUsers(List<User> wishfulUsers) {
		this.wishfulUsers = wishfulUsers;
	}

	public User addWishfulUser(User user) {
		getWishfulUsers().add(user);
		return user;
	}

	public User removeWishfulUser(User user) {
		getWishfulUsers().remove(user);
		return user;
	}

	public List<UserBook> getUserBooks() {
		return userBooks;
	}

	public void setUserBooks(List<UserBook> userBooks) {
		this.userBooks = userBooks;
	}

	public UserBook addUserBook(UserBook userBook) {
		getUserBooks().add(userBook);
		userBook.setBook(this);
		return userBook;
	}

	public UserBook removeUserBook(UserBook userBook) {
		getUserBooks().remove(userBook);
		userBook.setBook(null);
		return userBook;
	}

	public List<BookRequest> getBookRequests() {
		return bookRequests;
	}

	public void setBookRequests(List<BookRequest> bookRequests) {
		this.bookRequests = bookRequests;
	}

	public BookRequest addBookRequest(BookRequest bookRequest) {
		getBookRequests().add(bookRequest);
		bookRequest.setBook(this);
		return bookRequest;
	}

	public BookRequest removeBookRequest(BookRequest bookRequest) {
		getBookRequests().remove(bookRequest);
		bookRequest.setBook(null);
		return bookRequest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}