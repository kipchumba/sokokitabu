package group.assignment.sokokitabu.entities;

public enum Inbox {
    SwapRequest("Swap Request"), PurchaseRequest("Purchase Request"), CancelledPurchaseRequest(
	    "Cancelled Purchase RequestCan"), CancelledSwapRequest(
	    "Cancelled Swap Request");
    private String name;

    private Inbox(String name) {
	this.name = name;
    }

    public String getName() {
	return this.name;
    }

}
