package group.assignment.sokokitabu.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the user_book_price database table.
 * 
 */
@Entity
@Table(name = "User_Book")
@NamedQueries({
		@NamedQuery(name = "UserBookPrice.findAll", query = "SELECT u FROM UserBook u"),
		@NamedQuery(name = UserBook.FIND_BY_USER_AND_BOOK, query = "SELECT u FROM UserBook u WHERE (u.book.id = :bookid AND u.user.email = :userid)") })
public class UserBook implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	private Integer id;

	private double price;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "UserId")
	private User user;

	// bi-directional many-to-one association to Book
	@ManyToOne
	@JoinColumn(name = "BookId")
	private Book book;

	public static final String FIND_BY_USER_AND_BOOK = "findByUserAndBook";

	public UserBook() {
	}

	public UserBook(double price, User user, Book book) {
		this.price = price;
		this.user = user;
		this.book = book;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}