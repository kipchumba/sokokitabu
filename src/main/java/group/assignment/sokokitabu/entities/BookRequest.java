package group.assignment.sokokitabu.entities;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the book_request database table.
 * 
 */
@Entity
@Table(name = "book_request")
@NamedQuery(name = "BookRequest.findAll", query = "SELECT b FROM BookRequest b")
public class BookRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(updatable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date requestTime;

	@Column(updatable = false)
	@Enumerated(EnumType.STRING)
	private RequestType requestType;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "ownerId")
	private User owner;

	// bi-directional many-to-one association to Book
	@ManyToOne
	@JoinColumn(name = "ownerBookId")
	private Book book;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "requesterId")
	private User requester;

	// bi-directional many-to-one association to UserInbox
	@OneToOne(mappedBy = "bookRequest", cascade = CascadeType.PERSIST)
	private UserInbox userInbox;

	public BookRequest() {
	}

	public BookRequest(RequestType requestType, User owner, Book book,
			User requester) {
		this.requestType = requestType;
		setOwner(owner);
		setBook(book);
		setRequester(requester);
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getRequestTime() {
		return this.requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	public RequestType getRequestType() {
		return this.requestType;
	}

	public void setRequestType(RequestType requestType) {
		this.requestType = requestType;
	}

	public User getOwner() {
		return this.owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getRequester() {
		return this.requester;
	}

	public void setRequester(User requester) {
		this.requester = requester;
	}

	public UserInbox getUserInbox() {
		return this.userInbox;
	}

	public void setUserInbox(UserInbox userInbox) {
		this.userInbox = userInbox;
	}

	@PrePersist
	public void initializeDate() {
		this.requestTime = new Date();
	}

}
