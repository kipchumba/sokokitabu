package group.assignment.sokokitabu.entities;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the address database table.
 * 
 */
@Entity
@NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String userId;

	@Column(nullable = false)
	private String box;

	@Column(name = "MobileNumber", nullable = false)
	private String mobileNumber;

	@Column(nullable = false)
	private String postalCode;

	@Column(nullable = false)
	private String town;

	// bi-directional one-to-one association to User
	@OneToOne
	@JoinColumn(name = "UserId", nullable = false)
	private User user;

	public Address() {
	}

	public Address(String box, String mobileNumber, String postalCode,
			String town) {
		this.box = box;
		this.mobileNumber = mobileNumber;
		this.postalCode = postalCode;
		this.town = town;
	}

	public String getUserEmail() {
		return this.userId;
	}

	public void setUserEmail(String userEmail) {
		this.userId = userEmail;
	}

	public String getBox() {
		return this.box;
	}

	public void setBox(String box) {
		this.box = box;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getTown() {
		return this.town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}