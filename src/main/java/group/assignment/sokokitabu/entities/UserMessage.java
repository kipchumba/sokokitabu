package group.assignment.sokokitabu.entities;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the usermessage database table.
 * 
 */
@Entity
@Table(name = "User_Message")
@NamedQuery(name = UserMessage.FIND_ALL, query = "SELECT u FROM UserMessage u")
public class UserMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private Integer id;

    @Lob
    private String message;

    private String userEmail;

    private String userName;

    public static final String FIND_ALL = "Usermessage.findAll";

    public UserMessage() {
    }

    public UserMessage(String message, String userEmail, String userName) {
	this.message = message;
	this.userEmail = userEmail;
	this.userName = userName;
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getMessage() {
	return this.message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public String getUserEmail() {
	return this.userEmail;
    }

    public void setUserEmail(String userEmail) {
	this.userEmail = userEmail;
    }

    public String getUserName() {
	return this.userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

}