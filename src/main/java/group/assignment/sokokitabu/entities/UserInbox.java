package group.assignment.sokokitabu.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the userinbox database table.
 * 
 */
@Entity
@Table(name = "User_Inbox")
@NamedQuery(name = "UserInbox.findAll", query = "SELECT u FROM UserInbox u")
public class UserInbox implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(updatable = false)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(updatable = false)
	private Date dateOfMessage;

	@Enumerated(EnumType.STRING)
	private Inbox message;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "UserId")
	private User user;

	@Column(updatable = true)
	@Enumerated(EnumType.STRING)
	private ReadStatus readStatus;

	// bi-directional many-to-one association to BookRequest
	@OneToOne
	@JoinColumn(name = "requestId", nullable = false)
	private BookRequest bookRequest;

	public UserInbox() {
	}

	public UserInbox(Inbox message, User user) {
		this.message = message;
		this.user = user;
	}

	public UserInbox(Inbox message, User user, ReadStatus read) {
		this.message = message;
		this.user = user;
		this.readStatus = read;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateOfMessage() {
		return this.dateOfMessage;
	}

	public void setDateOfMessage(Date dateOfMessage) {
		this.dateOfMessage = dateOfMessage;
	}

	public Inbox getMessage() {
		return this.message;
	}

	public void setMessage(Inbox message) {
		this.message = message;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ReadStatus getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(ReadStatus readStatus) {
		this.readStatus = readStatus;
	}

	public BookRequest getBookRequest() {
		return bookRequest;
	}

	public void setBookRequest(BookRequest bookRequest) {
		this.bookRequest = bookRequest;
		bookRequest.setUserInbox(this);
	}

	@PrePersist
	public void initializeDate() {
		this.dateOfMessage = new Date();
	}

}
