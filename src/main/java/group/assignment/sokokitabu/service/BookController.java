package group.assignment.sokokitabu.service;

import group.assignment.sokokitabu.entities.Book;
import group.assignment.sokokitabu.entities.Category;
import group.assignment.sokokitabu.entities.Level;
import group.assignment.sokokitabu.exceptions.BookException;
import group.assignment.sokokitabu.exceptions.CategoryException;
import group.assignment.sokokitabu.jpa.BookEAO;
import group.assignment.sokokitabu.jpa.CategoryEAO;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

@Stateless
public class BookController {

	private static final String DUPLICATE_BOOK = "internalError";

	private static final String NON_EXISTING_BOOK_ON_DELETE = "internalError";

	private static final String NON_EXISTING_BOOK_ON_UPDATE = "internalError";

	private static final String DUPLICATE_CATEGORY = "internalError";

	private static final String NON_EXISTING_CATEGORY_ON_DELETE = "internalError";

	private static final String NON_EXISTING_CATEGORY_ON_UPDATE = "internalError";

	@EJB
	BookEAO bookEao;

	@EJB
	CategoryEAO categoryEao;

	private static Logger logger = Logger
			.getLogger("group.assignment.sokokitabu.exceptions.service.BookController");

	public Book createBook(@NotNull final Book book) throws BookException {

		Book existingBook = null;
		if (book.getId() != null) {
			existingBook = bookEao.findById(book.getId());
		}

		if (existingBook != null) {
			logger.fine("Attempted to create a duplicate book.");
			throw new BookException(DUPLICATE_BOOK);
		} else {
			return bookEao.create(book);
		}

	}

	public void removeBook(@NotNull final Book book) throws BookException {

		Book existingBook = bookEao.findById(book.getId());
		if (existingBook == null) {
			logger.fine("Attempted to delete non-existing book.");
			throw new BookException(NON_EXISTING_BOOK_ON_DELETE);
		} else {
			bookEao.delete(book);
		}
	}

	public Book updateBook(@NotNull final Book book) throws BookException {

		Book existingBook = bookEao.findById(book.getId());
		if (existingBook == null) {
			logger.fine("Attempted to modify non-existing book.");
			throw new BookException(NON_EXISTING_BOOK_ON_UPDATE);
		} else {
			return bookEao.modify(book);
		}
	}

	public Category createCategory(@NotNull final Category category)
			throws CategoryException {

		Category existingCategory = null;
		if (category.getId() != null) {
			existingCategory = categoryEao.findById(category.getId());
		}
		if (existingCategory != null) {
			logger.fine("Attempted to create a duplicate category.");
			throw new CategoryException(DUPLICATE_CATEGORY);
		} else {
			return categoryEao.create(category);
		}

	}

	public void removeCategory(@NotNull final Category category)
			throws CategoryException {

		Category existingCategory = categoryEao.findById(category.getId());
		if (existingCategory == null) {
			logger.fine("Attempted to delete non-existing category.");
			throw new CategoryException(NON_EXISTING_CATEGORY_ON_DELETE);
		} else {
			categoryEao.delete(category);
		}
	}

	public Category updateCategory(@NotNull final Category category)
			throws CategoryException {

		Category existingCategory = categoryEao.findById(category.getId());
		if (existingCategory == null) {
			logger.fine("Attempted to modify non-existing category.");
			throw new CategoryException(NON_EXISTING_CATEGORY_ON_UPDATE);
		} else {
			return categoryEao.modify(category);
		}
	}

	public Book findBookById(@NotNull Integer id) {
		return bookEao.findById(id);
	}

	public List<Book> findBookByTitle(@NotNull String title) {
		return bookEao.findByTitle(title);
	}

	public List<Book> findBookByPublisher(@NotNull String publisher) {
		return bookEao.findByPublisher(publisher);
	}

	public List<Book> findBookByLevel(@NotNull Level level) {
		return bookEao.findByLevel(level);
	}

	public List<Book> findBookByLevelAndSubLevel(@NotNull Level level,
			@NotNull String sublevel) {
		return bookEao.findByLevelAndSubLevel(level, sublevel);
	}

	public Category findCategoryById(@NotNull Integer id) {
		return categoryEao.findById(id);
	}

	public List<Category> findAll() {
		return categoryEao.findAll();
	}

	public List<Category> findCategoryByLevel(@NotNull Level level) {
		return categoryEao.findByLevel(level);
	}

	public List<Category> findCategoryByLevelAndSubLevel(@NotNull Level level,
			@NotNull String sublevel) {
		return categoryEao.findByLevelAndSubLevel(level, sublevel);
	}

	public Category findCategoryByLevelSubLevelAndSubject(@NotNull Level level,
			@NotNull String sublevel, @NotNull String subject) {
		return categoryEao.findByLevelSubLevelAndSubject(level, sublevel,
				subject);
	}

	public List<Book> getLatestBooks(int max, int start) {
		return bookEao.getLatestBooks(max, start);
	}

	public int getBookCount() {
		return bookEao.getBookCount();
	}

	public Book findBookByAll(@NotNull String title, @NotNull String publisher,
			@NotNull Category category) {
		return bookEao.findByAll(title, publisher, category);
	}

	public List<String> findDistinctSubjects(@NotNull Level level) {

		return categoryEao.findDistinctSubJects(level);
	}

	public List<String> findDistinctSubLevels(@NotNull Level level) {
		return categoryEao.findDistinctSubLevels(level);
	}

	public List<Book> findBookByLevelAndSubject(@NotNull Level level,
			@NotNull String subject) {
		return bookEao.findByLevelAndSubject(level, subject);
	}

	public List<Book> findBookByLevelSubLevelAndSubject(@NotNull Level level,
			@NotNull String sublevel, @NotNull String subject) {
		return bookEao.findByLevelSubLevelAndSubject(level, sublevel, subject);
	}

	public Book fullBook(@NotNull Integer bookid) {
		return bookEao.findFullyInitializedBookById(bookid);
	}

	public List<Category> findCategoryByLevelAndSubject(@NotNull Level level,
			@NotNull String subject) {
		return categoryEao.findByLevelAndSubject(level, subject);
	}
}
