package group.assignment.sokokitabu.service;

import group.assignment.sokokitabu.entities.Address;
import group.assignment.sokokitabu.entities.BookRequest;
import group.assignment.sokokitabu.entities.RequestType;
import group.assignment.sokokitabu.entities.User;
import group.assignment.sokokitabu.entities.UserBook;
import group.assignment.sokokitabu.entities.UserInbox;
import group.assignment.sokokitabu.entities.UserMessage;
import group.assignment.sokokitabu.entities.UserReview;
import group.assignment.sokokitabu.exceptions.UserException;
import group.assignment.sokokitabu.jpa.BookEAO;
import group.assignment.sokokitabu.jpa.UserEAO;
import group.assignment.sokokitabu.jpa.UserMessageEAO;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.constraints.NotNull;

@Stateless
public class UserController {

	private static final String DUPLICATE_USER = "internalError";

	private static final String NON_EXISTING_USER_ON_DELETE = "internalError";

	private static final String NON_EXISTING_USER_ON_UPDATE = "internalError";

	@EJB
	UserEAO userEao;

	@EJB
	UserMessageEAO userMessageEao;

	@EJB
	BookEAO bookEao;

	private static Logger logger = Logger
			.getLogger("group.assignment.sokokitabu.exceptions.service.UserController");

	public UserController() {

	}

	public User createUser(@NotNull final User user) throws UserException {

		User existingUser = userEao.findById(user.getEmail());
		if (existingUser != null) {
			logger.fine("Attempted to create a duplicate user.");
			throw new UserException(DUPLICATE_USER);
		} else {
			return userEao.create(user);

		}

	}

	public User saveAddress(User user, Address address) {
		userEao.saveAddress(address, user);
		return userEao.findFullyInitializedUserById(user.getEmail());
	}

	public void removeUser(@NotNull final User user) throws UserException {

		User existingUser = userEao.findById(user.getEmail());
		if (existingUser == null) {
			logger.fine("Attempted to delete non-existing user.");
			throw new UserException(NON_EXISTING_USER_ON_DELETE);
		} else {
			userEao.delete(user);
		}
	}

	public User updateUser(@NotNull final User user) throws UserException {

		User existingUser = userEao.findById(user.getEmail());
		if (existingUser == null) {
			logger.fine("Attempted to modify non-existing book.");
			throw new UserException(NON_EXISTING_USER_ON_UPDATE);
		} else {
			return userEao.modify(user);
		}
	}

	public boolean doesUserNameExist(@NotNull String username) {
		return userEao.findByUsername(username) != null;
	}

	public boolean doesAnotherUserHasThisEmail(User me, String email) {
		User you = findByEmail(email);
		if (you == null) {
			return false;
		}

		return !me.equals(you);
	}

	public boolean doesAnotherUserHasThisUsernam(User me, String username) {
		User you = findByUsername(username);
		if (you == null) {
			return false;
		}

		return !me.equals(you);
	}

	public boolean doesEmailExist(@NotNull String email) {
		return userEao.findByEmail(email) != null;
	}

	public User findByUsername(@NotNull String username) {
		return userEao.findByUsername(username);
	}

	public User findByEmail(@NotNull String email) {
		return userEao.findByEmail(email);
	}

	public List<User> findAll() {
		return userEao.findAll();
	}

	public User findUserByUsernameAndPassword(@NotNull String username,
			@NotNull String password) {
		return userEao.findByUsernameAndPassword(username, password);
	}

	public User findUserByEmailAndPassword(@NotNull String email,
			@NotNull String password) {
		return userEao.findByEmailAndPassword(email, password);
	}

	public boolean isPasswordCorrectForUsername(@NotNull String username,
			@NotNull String password) {
		return userEao.findByUsernameAndPassword(username, password) != null;
	}

	public boolean isPasswordCorrectForEmail(@NotNull String email,
			@NotNull String password) {
		return userEao.findByEmailAndPassword(email, password) != null;
	}

	public UserMessage createUserMessage(@NotNull final UserMessage userMessage) {
		return userMessageEao.create(userMessage);

	}

	public void removeUserBook(@NotNull UserBook userbook) {
		userEao.deleteUserBook(userbook);
	}

	public void removeUserInbox(@NotNull UserInbox inbox) {
		userEao.deleteUserInbox(inbox);
	}

	public void removeBookInWishlist(@NotNull String userid, Integer bookid) {
		userEao.deleteBookInUserWishlist(userid, bookid);
	}

	public void removeUserReview(@NotNull UserReview userReview) {
		userEao.deleteUserReview(userReview);
	}

	public void removeBookRequest(@NotNull BookRequest bookRequest) {
		userEao.deleteBookRequest(bookRequest);
	}

	public User fullUser(@NotNull User user) {
		return userEao.findFullyInitializedUserById(user.getEmail());
	}

	public BookRequest saveBookRequest(@NotNull BookRequest request) {
		return userEao.saveBookRequest(request);

	}

	public void saveUserInbox(@NotNull UserInbox userInbox) {
		userEao.saveUserInbox(userInbox);

	}

	public void saveUserReview(@NotNull UserReview userReview) {
		userEao.saveUserReview(userReview);
	}

	public void saveUserBook(@NotNull UserBook userBook) {
		userEao.saveUserBook(userBook);
	}

	public void saveBookToWishlist(@NotNull String userid,
			@NotNull Integer bookid) {
		userEao.saveBookToWishlist(userid, bookid);

	}

	public void updateReadStatusOfUserInbox(@NotNull UserInbox userInbox) {
		userEao.updateUserInbox(userInbox);
	}

	public boolean hasUserAlreadyMadeSuchRequest(String ownerid,
			String requesterid, Integer bookid, RequestType requesttype) {
		return userEao.isThereSuchRequest(ownerid, requesterid, bookid,
				requesttype);
	}
}
