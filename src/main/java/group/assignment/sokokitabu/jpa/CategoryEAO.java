package group.assignment.sokokitabu.jpa;

import group.assignment.sokokitabu.entities.Category;
import group.assignment.sokokitabu.entities.Level;
import group.assignment.sokokitabu.entities.User;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

@Stateless
@LocalBean
public class CategoryEAO implements GenericEAO<Category, Integer> {

	@PersistenceContext
	private EntityManager em;

	public CategoryEAO() {

	}

	public CategoryEAO(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional
	public Category create(Category entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	@Transactional
	public Category modify(Category entity) {
		Category mergedCategory = em.merge(entity);
		return mergedCategory;
	}

	@Override
	@Transactional
	public void delete(Category entity) {
		Category foundCategory = em.find(Category.class, entity.getId());
		em.remove(foundCategory);

	}

	@Override
	public Category findById(Integer id) {
		return em.find(Category.class, id);
	}

	@Override
	public List<Category> findAll() {
		TypedQuery<Category> findAllCategorysQuery = em.createNamedQuery(
				User.FIND_ALL, Category.class);
		List<Category> resultList = findAllCategorysQuery.getResultList();
		return resultList;
	}

	public List<Category> findByLevel(Level level) {
		TypedQuery<Category> findCategoriesQuery = em.createNamedQuery(
				Category.FIND_BY_LEVEL, Category.class);
		findCategoriesQuery.setParameter("level", level);
		List<Category> resultList = findCategoriesQuery.getResultList();
		return resultList;
	}

	public List<Category> findByLevelAndSubLevel(Level level, String subLevel) {
		TypedQuery<Category> findCategoriesQuery = em.createNamedQuery(
				Category.FIND_BY_LEVEL_AND_SUBLEVEL, Category.class);
		findCategoriesQuery.setParameter("level", level);
		findCategoriesQuery.setParameter("sublevel", subLevel);
		List<Category> resultList = findCategoriesQuery.getResultList();
		return resultList;
	}

	public Category findByLevelSubLevelAndSubject(Level level, String subLevel,
			String subject) {
		TypedQuery<Category> findCategoryQuery = em.createNamedQuery(
				Category.FIND_BY_LEVEL_SUBLEVEL_AND_SUBJECT, Category.class);
		findCategoryQuery.setParameter("level", level);
		findCategoryQuery.setParameter("sublevel", subLevel);
		findCategoryQuery.setParameter("subject", subject);
		Category category;
		try {
			category = findCategoryQuery.getSingleResult();

		} catch (NoResultException | NonUniqueResultException e) {
			category = null;
		}
		return category;
	}

	public List<Category> findByLevelAndSubject(Level level, String subject) {
		TypedQuery<Category> findCategoryQuery = em.createNamedQuery(
				Category.FIND_BY_LEVEL_AND_SUBJECT, Category.class);
		findCategoryQuery.setParameter("level", level);
		findCategoryQuery.setParameter("subject", subject);
		return findCategoryQuery.getResultList();
	}

	public List<String> findDistinctSubLevels(Level level) {
		TypedQuery<String> findDistinctSubLevelsQuery = em.createNamedQuery(
				Category.FIND_DISTINCT_SUBLEVELS_OF_LEVEL, String.class);
		findDistinctSubLevelsQuery.setParameter("level", level);
		List<String> resultList = findDistinctSubLevelsQuery.getResultList();
		return resultList;
	}

	public List<String> findDistinctSubJects(Level level) {
		TypedQuery<String> findDistinctSubJectsQuery = em.createNamedQuery(
				Category.FIND_DISTINCT_SUBJECTS_OF_LEVEL, String.class);
		findDistinctSubJectsQuery.setParameter("level", level);
		List<String> resultList = findDistinctSubJectsQuery.getResultList();
		return resultList;
	}
}
