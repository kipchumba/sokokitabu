package group.assignment.sokokitabu.jpa;

import group.assignment.sokokitabu.entities.Book;
import group.assignment.sokokitabu.entities.Category;
import group.assignment.sokokitabu.entities.LatestBook;
import group.assignment.sokokitabu.entities.Level;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.hibernate.Hibernate;

@Stateless
@LocalBean
public class BookEAO implements GenericEAO<Book, Integer> {

	@PersistenceContext
	private EntityManager em;

	public BookEAO() {

	}

	public BookEAO(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional
	public Book create(Book entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	@Transactional
	public Book modify(Book entity) {
		Book mergedBook = em.merge(entity);
		return mergedBook;
	}

	@Override
	@Transactional
	public void delete(Book entity) {
		Book foundBook = em.find(Book.class, entity.getId());
		em.remove(foundBook);

	}

	@Override
	public Book findById(Integer id) {
		return em.find(Book.class, id);
	}

	public Book findByAll(String title, String publisher, Category category) {
		TypedQuery<Book> findBookQuery = em.createNamedQuery(Book.FIND_BY_ALL,
				Book.class);
		findBookQuery.setParameter("title", title);
		findBookQuery.setParameter("publisher", publisher);
		findBookQuery.setParameter("category", category);
		Book book;

		try {
			book = findBookQuery.getSingleResult();

		} catch (NoResultException e) {
			book = null;
		}
		return book;
	}

	@Override
	public List<Book> findAll() {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(Book.FIND_ALL,
				Book.class);
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> findByTitle(String title) {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(
				Book.FIND_BY_TITLE_LIKE, Book.class);
		findAllBooksQuery.setParameter("title", "%" + title + "%");
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> findByPublisher(String publisher) {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(
				Book.FIND_BY_PUBLISHER_LIKE, Book.class);
		findAllBooksQuery.setParameter("publisher", "%" + publisher + "%");
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> findByLevel(Level level) {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(
				Book.FIND_BY_LEVEL, Book.class);
		findAllBooksQuery.setParameter("level", level);
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> findByLevelAndSubLevel(Level level, String sublevel) {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(
				Book.FIND_BY_LEVEL_AND_SUBLEVEL, Book.class);
		findAllBooksQuery.setParameter("level", level);
		findAllBooksQuery.setParameter("sublevel", sublevel);
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> findByLevelAndSubject(Level level, String subject) {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(
				Book.FIND_BY_LEVEL_AND_SUBJECT, Book.class);
		findAllBooksQuery.setParameter("level", level);
		findAllBooksQuery.setParameter("subject", subject);
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> findByLevelSubLevelAndSubject(Level level,
			String sublevel, String subject) {
		TypedQuery<Book> findAllBooksQuery = em.createNamedQuery(
				Book.FIND_BY_LEVEL_SUBLEVEL_AND_SUBJECT, Book.class);
		findAllBooksQuery.setParameter("level", level);
		findAllBooksQuery.setParameter("sublevel", sublevel);
		findAllBooksQuery.setParameter("subject", subject);
		List<Book> resultList = findAllBooksQuery.getResultList();
		return resultList;
	}

	public List<Book> getLatestBooks(int max, int start) {
		TypedQuery<LatestBook> findLatestBooksQuery = em.createNamedQuery(
				LatestBook.FIND_ALL, LatestBook.class);
		findLatestBooksQuery.setFirstResult(start);
		findLatestBooksQuery.setMaxResults(max);
		List<LatestBook> resultList = findLatestBooksQuery.getResultList();
		List<Book> latestBooks = new ArrayList<>();
		for (LatestBook book : resultList) {
			latestBooks.add(book.getBook());
		}

		return latestBooks;
	}

	public int getBookCount() {
		Long value = (Long) em.createQuery("SELECT COUNT(*) FROM Book")
				.getSingleResult();
		return value.intValue();

	}

	public Book findFullyInitializedBookById(Integer bookid) {
		Book book = em.find(Book.class, bookid);

		if (book == null) {
			return null;
		}
		Hibernate.initialize(book.getUserBooks());
		return book;

	}

}
