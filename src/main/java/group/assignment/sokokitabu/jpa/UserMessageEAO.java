package group.assignment.sokokitabu.jpa;

import group.assignment.sokokitabu.entities.UserMessage;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
@LocalBean
public class UserMessageEAO implements GenericEAO<UserMessage, Integer> {

    @PersistenceContext
    EntityManager em;

    public UserMessageEAO() {
    }

    public UserMessageEAO(EntityManager em) {
	this.em = em;
    }

    @Override
    public UserMessage create(UserMessage entity) {
	em.persist(entity);
	return entity;
    }

    @Override
    public UserMessage modify(UserMessage entity) {
	UserMessage mergedUserMessage = em.merge(entity);
	return mergedUserMessage;
    }

    @Override
    public void delete(UserMessage entity) {
	UserMessage foundUserMessage = em.find(UserMessage.class,
		entity.getId());
	em.remove(foundUserMessage);
    }

    @Override
    public UserMessage findById(Integer id) {
	return em.find(UserMessage.class, id);
    }

    @Override
    public List<UserMessage> findAll() {
	TypedQuery<UserMessage> findAllUserMessagesQuery = em.createNamedQuery(
		UserMessage.FIND_ALL, UserMessage.class);
	List<UserMessage> resultList = findAllUserMessagesQuery.getResultList();
	return resultList;
    }

}
