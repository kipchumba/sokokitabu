package group.assignment.sokokitabu.jpa;

import group.assignment.sokokitabu.entities.Address;
import group.assignment.sokokitabu.entities.BookRequest;
import group.assignment.sokokitabu.entities.ReadStatus;
import group.assignment.sokokitabu.entities.RequestType;
import group.assignment.sokokitabu.entities.User;
import group.assignment.sokokitabu.entities.UserBook;
import group.assignment.sokokitabu.entities.UserInbox;
import group.assignment.sokokitabu.entities.UserReview;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.hibernate.Hibernate;

@Stateless
@LocalBean
public class UserEAO implements GenericEAO<User, String> {

	final String DELETE_USER_BOOK = "DELETE FROM User_Book WHERE UserId=? AND BookId=?";
	final String DELETE_BOOK_FROM_USER_WISHLIST = "DELETE FROM User_Wishlist WHERE UserId=? AND BookId=?";
	final String INSERT_BOOK_INTO_USER_WISHLIST = "INSERT INTO User_Wishlist VALUES (?,?)";
	final String DELETE_USER_BOOK_REQUEST = "DELETE FROM Book_Request WHERE OwnerId=? AND OwnerBookId=? AND RequesterId=?";
	final String DELETE_USER_REVIEW = "DELETE FROM User_Review WHERE ReviewerId=? AND ReviewedUserId=?";
	final String DELETE_USER_INBOX = "DELETE FROM User_Inbox WHERE Id=?";
	final String UPDATE_USER_INBOX = "UPDATE User_Inbox SET ReadStatus=? WHERE Id=?";
	final String SELECT_SUCH_BOOK_REQUEST = "SELECT * FROM Book_Request WHERE OwnerId=? AND RequesterId=? AND OwnerBookId=? AND RequestType=?";
	final String INSERT_USER_INBOX = "INSERT INTO User_Inbox VALUES (?,?,?,?,?,?)";
	final String INSERT_USER_ADDRESS = "INSERT INTO Address VALUES (?,?,?,?,?)";

	@PersistenceContext
	private EntityManager em;

	public UserEAO() {

	}

	public UserEAO(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional
	public User create(User entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	@Transactional
	public User modify(User entity) {
		User mergedUser = em.merge(entity);
		return mergedUser;
	}

	@Override
	@Transactional
	public void delete(User entity) {
		User foundUser = em.find(User.class, entity.getEmail());
		em.remove(foundUser);

	}

	@Override
	public User findById(String id) {
		User user = em.find(User.class, id);
		return user;
	}

	@Override
	public List<User> findAll() {
		TypedQuery<User> findAllUsersQuery = em.createNamedQuery(User.FIND_ALL,
				User.class);
		List<User> resultList = findAllUsersQuery.getResultList();
		return resultList;
	}

	public User findByEmail(String email) {
		TypedQuery<User> findUserQuery = em.createNamedQuery(
				User.FIND_BY_EMAIL, User.class);
		findUserQuery.setParameter("email", email);
		User user;
		try {
			user = findUserQuery.getSingleResult();

		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	public User findByUsername(String username) {
		TypedQuery<User> findUserQuery = em.createNamedQuery(
				User.FIND_BY_USERNAME, User.class);
		findUserQuery.setParameter("username", username);
		User user;

		try {
			user = findUserQuery.getSingleResult();

		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	public User findByUsernameAndPassword(String username, String password) {
		TypedQuery<User> findUserQuery = em.createNamedQuery(
				User.FIND_BY_USERNAME_AND_PASSWORD, User.class);
		findUserQuery.setParameter("username", username);
		findUserQuery.setParameter("password", password);
		User user;
		try {
			user = findUserQuery.getSingleResult();

		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	public User findByEmailAndPassword(String email, String password) {
		TypedQuery<User> findUserQuery = em.createNamedQuery(
				User.FIND_BY_EMAIL_AND_PASWWORD, User.class);
		findUserQuery.setParameter("email", email);
		findUserQuery.setParameter("password", email);
		User user;
		try {
			user = findUserQuery.getSingleResult();

		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	public UserBook saveUserBook(UserBook userbook) {
		em.persist(userbook);
		return userbook;
	}

	public BookRequest saveBookRequest(BookRequest request) {
		em.persist(request);
		return request;
	}

	public UserReview saveUserReview(UserReview userReview) {
		em.persist(userReview);
		return userReview;
	}

	public void saveUserInbox(UserInbox userInbox) {
		Query query = em.createNativeQuery(INSERT_USER_INBOX);
		query.setParameter(1, null);
		query.setParameter(2, userInbox.getUser().getEmail());
		query.setParameter(3, userInbox.getMessage().toString());
		query.setParameter(4, userInbox.getBookRequest().getRequestTime());
		query.setParameter(5, userInbox.getReadStatus().toString());
		query.setParameter(6, userInbox.getBookRequest().getId());
		query.executeUpdate();
	}

	public void deleteUserBook(UserBook userbook) {
		UserBook foundUserBook = em.find(UserBook.class, userbook.getId());
		em.remove(foundUserBook);
	}

	public void deleteBookInUserWishlist(String useremail, Integer bookid) {
		Query query = em.createNativeQuery(DELETE_BOOK_FROM_USER_WISHLIST);
		query.setParameter(1, useremail);
		query.setParameter(2, bookid);
		query.executeUpdate();
	}

	public void deleteBookRequest(BookRequest request) {
		BookRequest foundBookRequest = em.find(BookRequest.class,
				request.getId());
		em.remove(foundBookRequest);
	}

	public void deleteUserInbox(UserInbox inbox) {
		UserInbox foundUserInbox = em.find(UserInbox.class, inbox.getId());
		em.remove(foundUserInbox);
	}

	public void deleteUserReview(UserReview review) {
		UserReview foundReview = em.find(UserReview.class, review.getId());
		em.remove(foundReview);
	}

	public User findFullyInitializedUserById(String email) {
		User user = findById(email);

		if (user == null) {
			return null;
		}

		Hibernate.initialize(user.getBookRequestsAsOwner());
		Hibernate.initialize(user.getBookRequestsAsRequester());
		Hibernate.initialize(user.getBookRequestsAsOwner());
		Hibernate.initialize(user.getBooksInWishList());
		Hibernate.initialize(user.getUserBooks());
		Hibernate.initialize(user.getUserInboxes());
		Hibernate.initialize(user.getReviewsByOthers());

		return user;
	}

	public void saveBookToWishlist(String userid, Integer bookid) {
		Query query = em.createNativeQuery(INSERT_BOOK_INTO_USER_WISHLIST);
		query.setParameter(1, userid);
		query.setParameter(2, bookid);
		query.executeUpdate();

	}

	public void updateUserInbox(UserInbox userInbox) {
		Query query = em.createNativeQuery(UPDATE_USER_INBOX);
		query.setParameter(1, ReadStatus.R.toString());
		query.setParameter(2, userInbox.getId());
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	public boolean isThereSuchRequest(String ownerid, String requesterid,
			Integer bookid, RequestType requesttype) {
		Query query = em.createNativeQuery(SELECT_SUCH_BOOK_REQUEST);
		query.setParameter(1, ownerid);
		query.setParameter(2, requesterid);
		query.setParameter(3, bookid);
		query.setParameter(4, requesttype.toString());
		List<Object> result = query.getResultList();

		return result.size() != 0;
	}

	public void saveAddress(Address address, User user) {
		Query query = em.createNativeQuery(INSERT_USER_ADDRESS);
		query.setParameter(1, user.getEmail());
		query.setParameter(2, address.getTown());
		query.setParameter(3, address.getBox());
		query.setParameter(4, address.getMobileNumber());
		query.setParameter(5, address.getPostalCode());
		query.executeUpdate();
	}
}
