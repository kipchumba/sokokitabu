package group.assignment.sokokitabu.jpa;

import java.util.List;

public interface GenericEAO<E, I> {

    /**
	 * Persist the entity
	 * 
	 * @param entity
	 * @return
	 */
	E create(E entity);

	/**
	 * Modify (merge any changes) an existing entity
	 * 
	 * @param entity
	 * @return
	 */
	E modify(E entity);

	/**
	 * Remove the entity from persistence
	 * 
	 * @param entity
	 */
	void delete(E entity);

	/**
	 * Find the Entity by it's primary key
	 * 
	 * @param id
	 * @return
	 */
	E findById(I id);

	/**
	 * Return all the entities in persistence
	 * 
	 * @return
	 */
	List<E> findAll();
}
