package group.assignment.sokokitabu.exceptions;

public class CategoryException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public CategoryException(String message) {
	super(message);
    }

    public CategoryException(Throwable cause) {
	super(cause);

    }

    public CategoryException(String message, Throwable cause) {
	super(message, cause);
    }

}
