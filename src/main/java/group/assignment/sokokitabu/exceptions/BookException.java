package group.assignment.sokokitabu.exceptions;


public class BookException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public BookException(String message, Throwable cause) {
	super(message, cause);
    }

    public BookException(String message) {
	super(message);
    }

    public BookException(Throwable cause) {
	super(cause);
    }

}
