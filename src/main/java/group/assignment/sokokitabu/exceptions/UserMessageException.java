package group.assignment.sokokitabu.exceptions;

public class UserMessageException extends ApplicationException {

    private static final long serialVersionUID = 1L;

    public UserMessageException(String message, Throwable cause) {
	super(message, cause);
    }

    public UserMessageException(String message) {
	super(message);
    }

    public UserMessageException(Throwable cause) {
	super(cause);
    }

}
