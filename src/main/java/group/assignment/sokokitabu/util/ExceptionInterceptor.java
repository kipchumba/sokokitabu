package group.assignment.sokokitabu.util;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * @author Antonio Goncalves http://www.antoniogoncalves.org -- This interceptor
 *         catches exception and displayes them in a JSF page
 */

@Interceptor
@CatchException
public class ExceptionInterceptor implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject
	private Logger log;

	@AroundInvoke
	public Object catchException(InvocationContext ic) throws Exception {
		try {
			return ic.proceed();
		} catch (Exception e) {
			addErrorMessage(e.getMessage());
			log.severe("/!\\ " + ic.getTarget().getClass().getName() + " - "
					+ ic.getMethod().getName() + " - " + e.getMessage());
		}
		return null;
	}

	protected void addErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		message = getI18nMessage("internalError");
		context.addMessage("global", new FacesMessage());
	}

	private String getI18nMessage(String msgKey) {
		ResourceBundle bundle = ResourceBundle.getBundle("messages");
		return bundle.getString(msgKey);
	}
}
