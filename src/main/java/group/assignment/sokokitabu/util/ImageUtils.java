package group.assignment.sokokitabu.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.primefaces.model.UploadedFile;

public class ImageUtils {

	private static final int IMG_WIDTH = 200;
	private static final int IMG_HEIGHT = 280;
	private static final String appFolderProperty = "soko.home";
	public static String extension = ".jpg";

	public static boolean bookHasImage(Integer bookid) {
		File folder = new File(System.getProperty(appFolderProperty));
		File file = new File(folder, bookid.toString() + extension);
		return file.exists();
	}

	public static void saveBookImage(UploadedFile file, Integer bookid)
			throws IOException {
		File folder = new File(System.getProperty(appFolderProperty));
		InputStream input = file.getInputstream();
		BufferedImage originalImage = ImageIO.read(input);
		int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
				: originalImage.getType();
		BufferedImage resizeImageJpgWithHint = resizeImageWithHint(
				originalImage, type);
		ImageIO.write(resizeImageJpgWithHint, extension, new File(folder,
				bookid.toString() + "." + extension));
	}

	private static BufferedImage resizeImageWithHint(
			BufferedImage originalImage, int type) {

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT,
				type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		return resizedImage;
	}

	public static String getBookImagePath(Integer bookid) {
		File folder = new File(System.getProperty(appFolderProperty));
		File file = new File(folder, bookid.toString() + extension);
		return file.toURI().toString();
	}

}
