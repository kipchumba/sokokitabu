package group.assignment.sokokitabu.web.views;

import group.assignment.sokokitabu.entities.Book;
import group.assignment.sokokitabu.entities.Level;
import group.assignment.sokokitabu.service.BookController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class BookBean extends AbstractBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private BookController bookCatalog;
	private int CAROUSEL_BOOKS_MAX = 6;
	private final String defaultSubject = "English";

	private String searchWord;
	private List<Book> books;

	private String subject;
	private String sublevel;
	private Level selectedLevel;

	private List<String> subjects;
	private List<String> subLevels;

	public String performSearch() {
		setBooks(bookCatalog.findBookByTitle(searchWord));
		return "searchresult.faces";
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public List<Book> getLatestBooks(int max, int start) {
		return bookCatalog.getLatestBooks(max, start);
	}

	public Level[] getLevels() {
		return Level.values();
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public void levelChanged(ValueChangeEvent e) {
		Level level = (Level) e.getNewValue();
		if (level != null) {
			switch (level) {
			case Higher:
				setSubjects(getHigherSubjects());
				break;
			case Secondary:
				setSubjects(getSecondarySubjects());
				setSubLevels(getSecondarySublevels());
				break;
			case Primary:
				setSubjects(getPrimarySubjects());
				setSubLevels(getPrimarySublevels());
				break;
			case Recreational:

				break;
			default:
				break;
			}
		}
	}

	public void subjectChanged(ValueChangeEvent e) {

		String subject = (String) e.getNewValue();
		setSubject(subject);
		if (selectedLevel == Level.Higher) {
			books = bookCatalog.findBookByLevelAndSubject(selectedLevel,
					subject);
		} else {
			books = bookCatalog.findBookByLevelSubLevelAndSubject(
					selectedLevel, sublevel, subject);

		}

	}

	public List<String> getSubLevels() {
		return subLevels;
	}

	public void setSubLevels(List<String> subLevels) {
		this.subLevels = subLevels;
	}

	public List<String> completeTitle(String query) {
		List<String> bookTitles = new ArrayList<>();
		for (Book book : bookCatalog.findBookByTitle(query)) {
			bookTitles.add(book.getTitle());
		}
		return bookTitles;
	}

	public List<String> completePublisher(String query) {
		List<String> bookPublishers = new ArrayList<>();
		for (Book book : bookCatalog.findBookByPublisher(query)) {
			bookPublishers.add(book.getPublisher());
		}
		return bookPublishers;
	}

	public List<String> getPrimarySublevels() {
		return bookCatalog.findDistinctSubLevels(Level.Primary);

	}

	public List<String> getSecondarySublevels() {
		return bookCatalog.findDistinctSubLevels(Level.Secondary);
	}

	public List<String> getPrimarySubjects() {
		return bookCatalog.findDistinctSubjects(Level.Primary);

	}

	public List<String> getSecondarySubjects() {
		return bookCatalog.findDistinctSubjects(Level.Secondary);
	}

	public List<String> getHigherSubjects() {
		return bookCatalog.findDistinctSubjects(Level.Higher);
	}

	public String selectSecondarySublevel(String sublevel) {
		setSelectedLevel(Level.Secondary);
		setSublevel(sublevel);
		books = bookCatalog.findBookByLevelAndSubLevel(Level.Secondary,
				sublevel);
		return "secondary.faces?faces-redirect=true";
	}

	public String selectPrimarySublevel(String sublevel) {
		setSelectedLevel(Level.Primary);
		setSublevel(sublevel);
		books = bookCatalog.findBookByLevelAndSubLevel(Level.Primary, sublevel);
		return "primary.faces?faces-redirect=true";
	}

	public String selectHigherSubject(String subject) {
		setSelectedLevel(Level.Higher);
		books = bookCatalog.findBookByLevelAndSubject(Level.Higher, subject);
		return "higher.faces?faces-redirect=true";
	}

	public String selectSecondaryLevel() {
		setSubject(defaultSubject);
		setSelectedLevel(Level.Secondary);
		books = bookCatalog.findBookByLevel(Level.Secondary);
		return "secondary.faces?faces-redirect=true";
	}

	public String selectPrimaryLevel() {
		setSubject(defaultSubject);
		setSelectedLevel(Level.Primary);
		books = bookCatalog.findBookByLevel(Level.Primary);
		return "primary.faces?faces-redirect=true";
	}

	public String selectHigherLevel() {
		setSelectedLevel(Level.Higher);
		books = bookCatalog.findBookByLevel(Level.Higher);
		return "higher.faces?faces-redirect=true";
	}

	public String selectRecreationalLevel() {
		setSelectedLevel(Level.Recreational);
		books = bookCatalog.findBookByLevel(Level.Recreational);
		return "recreational.faces?faces-redirect=true";
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Level getSelectedLevel() {
		return selectedLevel;
	}

	public void setSelectedLevel(Level selectedLevel) {
		this.selectedLevel = selectedLevel;
	}

	public String getSublevel() {
		return sublevel;
	}

	public void setSublevel(String sublevel) {
		this.sublevel = sublevel;
	}

	public List<Integer> getRandomBookNumbers() {
		Random random = new Random();
		int booksno = bookCatalog.getBookCount();
		List<Integer> list = new ArrayList<>();
		for (int x = 1; x <= CAROUSEL_BOOKS_MAX; x++) {
			list.add(random.nextInt(booksno) + 1);
		}
		return list;
	}
}
