package group.assignment.sokokitabu.web.views;

import group.assignment.sokokitabu.entities.Address;
import group.assignment.sokokitabu.entities.Book;
import group.assignment.sokokitabu.entities.BookRequest;
import group.assignment.sokokitabu.entities.Category;
import group.assignment.sokokitabu.entities.Inbox;
import group.assignment.sokokitabu.entities.Level;
import group.assignment.sokokitabu.entities.ReadStatus;
import group.assignment.sokokitabu.entities.RequestType;
import group.assignment.sokokitabu.entities.User;
import group.assignment.sokokitabu.entities.UserBook;
import group.assignment.sokokitabu.entities.UserInbox;
import group.assignment.sokokitabu.entities.UserMessage;
import group.assignment.sokokitabu.exceptions.BookException;
import group.assignment.sokokitabu.exceptions.UserException;
import group.assignment.sokokitabu.service.BookController;
import group.assignment.sokokitabu.service.MessageService;
import group.assignment.sokokitabu.service.UserController;
import group.assignment.sokokitabu.util.CatchException;
import group.assignment.sokokitabu.util.EmailValidator;
import group.assignment.sokokitabu.util.ImageUtils;
import group.assignment.sokokitabu.util.LoggedIn;
import group.assignment.sokokitabu.web.beans.CredentialsBean;
import group.assignment.sokokitabu.web.beans.NewBook;
import group.assignment.sokokitabu.web.beans.NewUserBean;
import group.assignment.sokokitabu.web.beans.UserMessageBean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
@CatchException
public class UserBean extends AbstractBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	UserController userController;

	@Inject
	BookController bookController;

	@Inject
	MessageService messageService;

	@Inject
	private NewBook newBook;

	@Inject
	CredentialsBean credentials;

	@Inject
	NewUserBean newUser;

	private UserInbox selectedInbox;

	private Book viewedBook;

	private boolean userIsNull;

	private boolean bookIsNull;

	private boolean loggedIn = false;

	@Inject
	UserMessageBean userMessage;

	EmailValidator emailValidator = new EmailValidator();

	private User viewedUser;

	@Produces
	@LoggedIn
	private User loggedInUser;

	public String logIn() {

		if (!userController.doesUserNameExist(credentials.getId())
				&& !userController.doesEmailExist(credentials.getId())) {
			addWarningMessage("signinform:usernameError", "id_not_exist");
			return null;
		}

		if (!userController.isPasswordCorrectForUsername(credentials.getId(),
				credentials.getPassword())
				&& !userController.isPasswordCorrectForEmail(
						credentials.getId(), credentials.getId())) {
			addWarningMessage("signinform:passwordError", "pwd_wrong");
			return null;
		}
		boolean isByEmail = emailValidator.validate(credentials.getId());
		if (isByEmail) {
			loggedInUser = userController.findUserByEmailAndPassword(
					credentials.getId(), credentials.getPassword());
		} else {
			loggedInUser = userController.findUserByUsernameAndPassword(
					credentials.getId(), credentials.getPassword());
		}

		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "signin_success");
		setLoggedIn(true);
		return "myaccount.faces?faces-redirect=true";
	}

	public String createNewUser() throws UserException {

		if (newUser.getGender() == null || newUser.getGender().equals("")) {
			addWarningMessage("signupform:gender", "gender_filled");
			return null;
		}

		if (userController.doesEmailExist(newUser.getEmail())) {
			addWarningMessage("signupform:email", "email_taken");
			return null;
		}

		if (userController.doesUserNameExist(newUser.getUsername())) {
			addWarningMessage("signupform:username", "username_taken");
			return null;
		}

		Address address = new Address(newUser.getBox(),
				newUser.getMobileNumber(), newUser.getPostalCode(),
				newUser.getTown());
		User createUser = new User(newUser.getEmail(), newUser.getFirstName(),
				newUser.getGender(), newUser.getLastName(),
				newUser.getPassword(), newUser.getUsername());

		loggedInUser = userController.createUser(createUser);
		loggedInUser = userController.saveAddress(loggedInUser, address);
		addInformationMessageToRedirect("global", "signup_success");
		setLoggedIn(true);
		return "main.faces?faces-redirect=true";
	}

	public String editUser() throws UserException {

		if (userController.doesAnotherUserHasThisEmail(loggedInUser,
				loggedInUser.getEmail())) {
			addWarningMessage("signupform:email", "email_taken");
			return null;
		}

		if (userController.doesAnotherUserHasThisUsernam(loggedInUser,
				loggedInUser.getUsername())) {
			addWarningMessage("signupform:username", "username_taken");
			return null;
		}

		loggedInUser = userController.updateUser(loggedInUser);
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "editprofile_success");
		return "myaccount.faces?faces-redirect=true";
	}

	public String addBook() throws BookException, UserException, IOException {
		if (newBook.getLevel() == null) {
			addWarningMessage("bookform:level", "level_filled");
			return null;
		}

		if ((newBook.getLevel().equals(Level.Primary)
				|| newBook.getLevel().equals(Level.Secondary) || newBook
				.getLevel().equals(Level.Higher))
				&& (newBook.getSubject() == null || newBook.getSubject()
						.equals(""))) {
			addWarningMessage("bookform:subject", "subject_filled");
			return null;
		}

		if ((newBook.getLevel().equals(Level.Primary) || newBook.getLevel()
				.equals(Level.Secondary))
				&& (newBook.getSublevel() == null || newBook.getSublevel()
						.equals(""))) {
			addWarningMessage("bookform:sublevel", "sublevel_filled");
			return null;
		}

		Category categoryOfBook;
		System.out.println("Level picked: " + newBook.getLevel()
				+ " Subject picked: " + newBook.getSubject()
				+ " sublevel picked: " + newBook.getSublevel());
		if (newBook.getLevel() == Level.Recreational) {
			categoryOfBook = bookController.findCategoryByLevel(
					Level.Recreational).get(0);
		} else if (newBook.getLevel() == Level.Higher) {
			categoryOfBook = bookController.findCategoryByLevelAndSubject(
					Level.Higher, newBook.getSubject()).get(0);
		} else {
			categoryOfBook = bookController
					.findCategoryByLevelSubLevelAndSubject(newBook.getLevel(),
							newBook.getSublevel(), newBook.getSubject());

		}

		Book toCreate = bookController.findBookByAll(newBook.getTitle(),
				newBook.getPublisher(), categoryOfBook);
		if (toCreate == null) {
			toCreate = new Book(newBook.getPublisher(), newBook.getTitle(),
					categoryOfBook);
			toCreate = bookController.createBook(toCreate);

		}
		if (newBook.getFile() != null
				&& !ImageUtils.bookHasImage(toCreate.getId())) {
			ImageUtils.saveBookImage(newBook.getFile(), toCreate.getId());
		}
		UserBook price = new UserBook(newBook.getPrice(), loggedInUser,
				toCreate);
		userController.saveUserBook(price);
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "book_add_success");
		return "mybooks.faces?faces-redirect=true";

	}

	public String removeUserBook(UserBook book) throws UserException {
		userController.removeUserBook(book);
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "book_remove_success");
		return "mybooks.faces?faces-redirect=true";
	}

	public String placeRequest(User owner, Book book, String requestType)
			throws UserException {

		if (!isLoggedIn()) {
			addInformationMessageToRedirect("global", "not_loggedin");
			return null;
		}

		if (owner == loggedInUser) {
			addInformationMessageToRedirect("global", "request_to_self");
			return null;
		}

		RequestType type = RequestType.valueOf(requestType);

		if (userController.hasUserAlreadyMadeSuchRequest(owner.getEmail(),
				loggedInUser.getEmail(), book.getId(), type)) {
			addInformationMessageToRedirect("global", "request_already_made");
			return null;
		}

		BookRequest bookRequest = new BookRequest(type, owner, book,
				loggedInUser);

		UserInbox ownerInbox;
		if (bookRequest.getRequestType().equals(RequestType.P)) {
			ownerInbox = new UserInbox(Inbox.PurchaseRequest, owner,
					ReadStatus.U);
		} else {
			ownerInbox = new UserInbox(Inbox.SwapRequest, owner, ReadStatus.U);
		}
		bookRequest = userController.saveBookRequest(bookRequest);
		ownerInbox.setBookRequest(bookRequest);
		userController.saveUserInbox(ownerInbox);
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "request_success");
		return null;
	}

	public String deleteInbox() throws UserException {
		BookRequest request = selectedInbox.getBookRequest();
		userController.removeUserInbox(selectedInbox);
		userController.removeBookRequest(request);
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "inbox_delete_success");
		return null;
	}

	public String logOut() {
		loggedInUser = null;
		addInformationMessageToRedirect("global", "signout_success");
		setLoggedIn(false);
		return "main.faces?faces-redirect=true";
	}

	public String deleteAccount() throws UserException {
		userController.removeUser(loggedInUser);
		loggedInUser = null;
		addInformationMessageToRedirect("global", "delete_account_success");
		return "main.faces?faces-redirect=true";
	}

	public String addBookToWishlist(Book book) throws UserException {

		if (!isLoggedIn()) {
			addInformationMessageToRedirect("global", "not_loggedin");
			return null;
		}

		if (loggedInUser.getBooksInWishList().contains(book)) {
			addInformationMessageToRedirect("global", "wishlist_add_error");
			return null;
		}

		for (UserBook userbook : loggedInUser.getUserBooks()) {

			if (userbook.getBook() == book) {
				addInformationMessageToRedirect("global", "wishlist_own_error");
				return null;
			}
		}
		userController
				.saveBookToWishlist(loggedInUser.getEmail(), book.getId());
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "wishlist_add_success");
		return null;
	}

	public String removeBookFromWishlist(Book book) throws UserException {
		userController.removeBookInWishlist(loggedInUser.getEmail(),
				book.getId());
		loggedInUser = userController.fullUser(loggedInUser);
		addInformationMessageToRedirect("global", "wishlist_remove_success");
		return null;
	}

	public String sendMessage() {

		if (userMessage.getHiddenValue() == null
				|| userMessage.getHiddenValue().equals("")) {
			UserMessage message = new UserMessage(userMessage.getMessage(),
					userMessage.getUserEmail(), userMessage.getUserName());
			userController.createUserMessage(message);
		}
		addInformationMessageToRedirect("global", "message_success");
		return "main.faces?faces-redirect=true";
	}

	public User getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}

	public String viewUser(User user) {

		if (!isLoggedIn()) {
			addInformationMessageToRedirect("global", "not_loggedin");
			return null;
		}

		user = userController.findByEmail(user.getEmail());
		if (user == null) {
			setUserIsNull(true);
		} else {
			user = userController.fullUser(user);
			setUserIsNull(false);
		}
		setViewedUser(user);
		return "viewuser.faces?faces-redirect=true";
	}

	public User getViewedUser() {
		return viewedUser;
	}

	public void setViewedUser(User viewedUser) {
		this.viewedUser = viewedUser;
	}

	public NewBook getNewBook() {
		return newBook;
	}

	public void setNewBook(NewBook newBook) {
		this.newBook = newBook;
	}

	public void priceChanged(ValueChangeEvent e) {

	}

	public String viewBook(Book book) {
		return viewBookFromId(book.getId());

	}

	public String viewBookFromId(Integer bookNo) {
		Book book = bookController.findBookById(bookNo);
		if (book == null) {
			setBookIsNull(true);
		} else {
			book = bookController.fullBook(bookNo);
			setBookIsNull(false);
		}
		setViewedBook(book);
		return "viewbook.faces?faces-redirect=true";

	}

	public Book getViewedBook() {
		return viewedBook;
	}

	public void setViewedBook(Book viewedBook) {
		this.viewedBook = viewedBook;
	}

	public boolean userHasBooks() {
		if (loggedInUser.getUserBooks() == null)
			return false;
		return loggedInUser.getUserBooks().size() != 0;
	}

	public boolean userHasEmptyWishlist() {
		if (loggedInUser.getBooksInWishList() == null)
			return false;
		return loggedInUser.getBooksInWishList().size() == 0;
	}

	public boolean isUserIsNull() {
		return userIsNull;
	}

	public void setUserIsNull(boolean userIsNull) {
		this.userIsNull = userIsNull;
	}

	public boolean isBookIsNull() {
		return bookIsNull;
	}

	public void setBookIsNull(boolean bookIsNull) {
		this.bookIsNull = bookIsNull;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public boolean isLoggedIn() {
		return loggedInUser != null;
	}

	public String goToAddBook() {
		return "addbook.faces?faces-redirect=true";
	}

	public UserInbox getSelectedInbox() {
		return selectedInbox;
	}

	public void setSelectedInbox(UserInbox selectedInbox) {
		this.selectedInbox = selectedInbox;
	}

	public String getUnreadUserInbox() {

		int number = 0;

		if (loggedInUser.getUserInboxes() == null)
			return String.valueOf(number);

		if (loggedInUser.getUserInboxes().size() == 0) {
			return String.valueOf(number);
		}
		for (int x = 0; x < loggedInUser.getUserInboxes().size(); x++) {
			if ((loggedInUser.getUserInboxes().get(x).getReadStatus() == ReadStatus.U)) {
				number++;
			}
		}

		return String.valueOf(number);
	}

	public boolean isFromRequester(Inbox inbox) {
		return (inbox == Inbox.PurchaseRequest || inbox == Inbox.SwapRequest);
	}

	public boolean selectedInboxFromRequester() {
		return isFromRequester(selectedInbox.getMessage());
	}

	public boolean userHasInbox() {
		if (loggedInUser.getUserInboxes() == null)
			return false;
		return loggedInUser.getUserInboxes().size() != 0;
	}

	public void updateReadStatus() {
		userController.updateReadStatusOfUserInbox(selectedInbox);
		loggedInUser = userController.fullUser(loggedInUser);
	}

	public int sortByLatestDateFirst(Object dateToTest1, Object dateToTest2) {
		Calendar date1 = Calendar.getInstance();
		Calendar date2 = Calendar.getInstance();
		date1.setTime((Date) dateToTest1);
		date2.setTime((Date) dateToTest2);

		if (date1 == date2)
			return 0;
		if (date1.before(date2))
			return -1;
		return 1;
	}
}
