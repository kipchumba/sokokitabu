package group.assignment.sokokitabu.web.beans;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named
public class UserMessageBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String message;

    private String userEmail;

    private String userName;
    
    private String hiddenValue;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHiddenValue() {
	return hiddenValue;
    }

    public void setHiddenValue(String hiddenValue) {
	this.hiddenValue = hiddenValue;
    }
    
    

}
