package group.assignment.sokokitabu.web.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.model.UploadedFile;

import group.assignment.sokokitabu.entities.Level;

@SessionScoped
@Named
public class NewBook implements Serializable{
    
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String title;
    private Level level;
    private String sublevel;
    private String subject;
    private String publisher;
    private UploadedFile file;
    private Double price;
    
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Level getLevel() {
        return level;
    }
    public void setLevel(Level level) {
        this.level = level;
    }
    public String getSublevel() {
        return sublevel;
    }
    public void setSublevel(String sublevel) {
        this.sublevel = sublevel;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getPublisher() {
        return publisher;
    }
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    public Integer getId() {
	return id;
    }
    public void setId(Integer id) {
	this.id = id;
    }
    public UploadedFile getFile() {
	return file;
    }
    public void setFile(UploadedFile file) {
	this.file = file;
    }
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
    
    

    
    
}
